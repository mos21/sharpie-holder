include <moslib/libchamfer.scad>;

include <peglib/defaults.scad>;
include <peglib/rail.scad>;

tallness = 3;
caps = 1;
cutout = true;

module foo() {}

height = base_size * tallness;

$fn = 200;

tube_outer_gap = 0;
cap_back_wall_gap = 0.2;
od = (base_size * 1) - tube_outer_gap;
id = od - (wall_thickness * 2);
chamfer = (wall_thickness / 2) / 2;

side_wall_gap = 0;


// move edge of tube against center line
translate([0, -od / 2, 0]) {

    translate([(base_size * -(caps - 1)) / 2, 0, 0])

for(c = [1:caps]) {
    translate([(base_size * (c - 1)), 0, 0])
        difference() {

            closed_chamfered_tube(height, od - side_wall_gap, id, chamfer = chamfer, cutout = true);
            
}

    }
}

rotate([0, 0, 180]) {
    rail(height = tallness, chamfer = chamfer, back_wall_gap = cap_back_wall_gap, copies = caps, side_wall_gap = side_wall_gap);
}


